var express = require('express');
var router = express.Router();
var models = require('../models');
var kue = require('kue'), jobs = kue.createQueue();

router.get('/', function (req, res, next) {
    res.format({
        json: function () {
            models.links.findAll().then(links => {
                res.json({
                    links: links
                });

            });
        },
        html: function () {
            models.links.findAll().then(links => {
                res.render('links/index', {
                    links: links
                });
            });
        }
    });
});

router.post('/', function (req, res, next) {
    var link = models.links.create(req.body);
    link.then(function (data) {
        //save id in REDIS       
        insertRedis(data.get('id'), data.get('link'),  data.get('parts'), data.get('minutes'), data.get('audio'),data.get('video') );
    });

    res.format({
        json: function () {
            link.then(links => {
                res.json({
                    links: links
                });
            });
        },
        html: function () {
            link.then(links => {
                res.render('links/index', {
                    links: links
                });
            });
        }
    });
});
function insertRedis(id, link,parts,min,audio,video) {
     var link = jobs.create('link', {
         id: id, link:link, parts:parts, min:min, audio:audio, video:video
     }).save(function (err) {
         if (!err) {
             console.log(link.id);
         }
     });
}
module.exports = router;
