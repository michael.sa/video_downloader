module.exports = function (sequelize, DataTypes) {
    var Link = sequelize.define('links', {
        link: DataTypes.STRING,
        status: DataTypes.STRING,
        video_name: DataTypes.STRING,
        video: DataTypes.BOOLEAN,
        audio: DataTypes.BOOLEAN,
        parts: DataTypes.INTEGER,
        minutes: DataTypes.STRING
    });

    return Link;
}