const {
    exec
} = require('child_process');
var models = require('./models');
var kue = require('kue'),
    jobs = kue.createQueue();
// route to download video and audio
var route = "/home/michael/Descargas/";
var nParts = 0;
var nMin = 0;

jobs.process('link', (job, done) => {
    console.log('id del job:' + job.id);
    console.log('id:' + job.data.id + ' ' + 'link: ' + job.data.link + ' parts:' + job.data.parts + ' min: ' + job.data.min + ' audio: ' + job.data.audio + ' video: ' + job.data.video);
    nParts = job.data.parts;
    nMin = job.data.min;
    // Do your task here
    if (job.data.audio) {
        inProgres(job.data.id);
        getAudio(job.data.link, job.data.id);
    }


    if (job.data.video) {
        inProgres(job.data.id);
        downloadVideo(job.data.link, job.data.id);
    }
    done();
});

function downloadVideo(link, idDB) {
    exec('youtube-dl -f 22 ' + link, (err, stdout, stderr) => {
        finalized(idDB);
        if (err) {
            // node couldn't execute the command
            console.log('Error: ' + err);
            error(idDB);
            return;
        }
        console.log('Video Downloaded!');
    });
};

function getFilaName(link, id) {
    var idLink = link.split("=")[1];
    exec('youtube-dl --get-filename -o "%(title)s.%(ext)s" ' + link, (err, stdout, stderr) => {
        if (err) {
            console.log('Error: ' + err);
            error(id);
            return;
        }
        //var fileName = stdout.substring(0, stdout.length - 6).concat('-' + idLink + '.mp3');
        var fileName = stdout.substring(0, stdout.length - 5).concat('.mp3');
        getSegundos(fileName, id);
    });
}

function getAudio(link, idDB) {
    exec('youtube-dl --extract-audio --audio-format mp3 --audio-quality 128K ' + link, (err, stdout, stderr) => {
        if (err) {
            console.log('Error: ' + err);
            error(idDB);
            return;
        }
        if (nParts != 0 || nMin != 0) {
            getFilaName(link, idDB);
        } else {
            console.log('Get Audio Ready.!!');
            finalized(idDB);
        }
    });
};

function recortAudioPart(audioName, timeS, id) {
    console.log('start cut Audio ');
    var split = 0;
    var segs = 0;
    segs = timeS;
    split = segs / nParts;
    var ss = 0;
    var t = split;
    var locationFile = route.concat(audioName);
    for (var index = 0; index < nParts; index++) {
        var name = "" + audioName.substring(0, audioName.length - 4) + index + ".mp3";
        var command = "ffmpeg -i \"" + locationFile + "\" -ss " + ss + " -t " + t + " " + route + "\"" + name + "\"";
        exec(command, (err, stdout, stderr) => {
            if (err) {
                console.log('Error: ' + err);
                error(id);
                return;
            }
        });
        ss = ss + split;
    }
    console.log('Audio Cut Ready.!!');
    finalized(id);
};

function recortAudioMin(audioName, timeS, id) {
    console.log('start cut Audio min ');
    var split = 0;
    var segs = 0;
    var minSeg = 0;
    segs = timeS;
    minSeg = 60 * nMin;
    split = segs / minSeg;
    var conSplit = split.toString();
    var nSplit = parseInt(conSplit.split(".")[0]);
    var ss = 0;
    var t = minSeg;
    var locationFile = route.concat(audioName);
    for (var index = 0; index <= nSplit; index++) {
        var name = "" + audioName.substring(0, audioName.length - 4) + index + ".mp3";
        exec("ffmpeg -i \"" + locationFile + "\" -ss " + ss + " -t " + t + " " + route + "\"" + name + "\"", (err, stdout, stderr) => {
            if (err) {
                console.log('Error: ' + err);
                error(id);
                return;
            }
        });
        ss = ss + minSeg;
    }
    console.log('Audio Cut Ready.!!');
    finalized(id);

};

function getSegundos(audioName, id) {
    var locationFile = route.concat(audioName);

    var command = "ffprobe \"" + locationFile + "\" -show_format 2>&1 | sed -n s/duration=//p";
    exec(command, (err, stdout, stderr) => {
        if (err) {
            console.log('Error: ' + err);
            error(id);
            return;
        }
        var timeS = parseInt(stdout.split(".")[0]);
        // console.log(`stdout: ${stdout}`);
        if (nParts != 0) {
            recortAudioPart(audioName, timeS, id);
        } else {
            if (timeS > 60) {
                recortAudioMin(audioName, timeS, id);
            }
        }
    });
};

function inProgres(id) {
    models.links.update({
        status: "in progress",
    }, {
        where: {
            id: id
        }
    });
};

function finalized(id) {
    models.links.update({
        status: "Downloaded",
    }, {
        where: {
            id: id
        }
    });
};

function error(id) {
    models.links.update({
        status: "Error",
    }, {
        where: {
            id: id
        }
    });
};