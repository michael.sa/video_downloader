var url = "";
var timer = new Timer();
var video = false;
var audio = false;
var Download = {
    getInfo: function () {
      return $.getJSON('http://localhost:3000/api/v1/links');  
    },
    sendData: function (data) {
        return $.post('http://localhost:3000/api/v1/links',data);
    } 
}

chrome.tabs.query({currentWindow: true, active: true}, function(tabs){
    url = document.getElementById('url').value = tabs[0].url;
    getDownloadsStatus();
    timer.start();
    timer.addEventListener('secondsUpdated', function (e) {
        if (timer.getTimeValues().toString(['seconds']) == '10') {
            getDownloadsStatus();
            timer.stop();
            timer.start();
        }
    });   
});

document.getElementById('folder').addEventListener('click', (e) => {
    chrome.downloads.showDefaultFolder();
})

document.getElementById('download').addEventListener('click', (e) => {
    chrome.tabs.executeScript({
        code: '(' + function(params) {
        return document.getElementById('eow-title').innerHTML;
        } + ')();'
    }, function(results) {
            var video_name = results[0];  
            var parts = 0;
            var minutes = 0;
            if(document.getElementById('parts').checked) {
                parts = document.getElementById('range').value;
            }else if(document.getElementById('minutes').checked) {
                minutes = document.getElementById('range').value;
            }
            if (video_name == null) {
                video_name = url;
            }
            Download.sendData({link: url, status: 'Waiting', video_name: video_name.replace(/[\r\n\s]/g, ''), video: document.getElementById('video').checked, audio: document.getElementById('audio').checked,
             parts: parts, minutes: minutes}).done(getDownloadsStatus);
        });
})

document.getElementById('parts').addEventListener('click', (e) => {
    document.getElementById('range-field').style.display = 'block';
});

document.getElementById('minutes').addEventListener('click', (e) => {
    document.getElementById('range-field').style.display = 'block';
});

document.getElementById('audio').addEventListener('click', (e) => {
    audio = document.getElementById('audio').checked;
    validate();
});

document.getElementById('video').addEventListener('click', (e) => {
     video = document.getElementById('video').checked; 
     validate();
});


function validate() {
    document.getElementById('options').style.display = 'none';
    document.getElementById('download-button').style.display = 'none';
    if (audio) {
        document.getElementById('options').style.display = 'block';
        document.getElementById('download-button').style.display = 'block';
    } else if (video) {
        document.getElementById('download-button').style.display = 'block';
        document.getElementById('options').style.display = 'none';
    } 
}

function getDownloadsStatus() {
    document.getElementById('row-container').innerHTML= '';
    Download.getInfo().done(function (json) {
        document.getElementById('row-container').style.display = 'block';
        json.links.forEach(function (link) {
            document.getElementById('row-container').innerHTML +=  
            "<div class='row' id='downloads'>" +
                "<div class='col s8'>" +
                    "<p><label id='name'>"+link.video_name+"</label></p>"+
                    "<label id='status'>" + 'Status: ' +link.status+ "</label>" +
                    "<div id=" +link.id+" class='progress'>"+
                      "<div class='indeterminate'></div>" +
                    "</div>"+
                    "<div class='progress' id="+ link.id + 'finish' + ">"+
                        "<div class='determinate' style='width: 100%'></div>" +
                    "</div>" +
                "</div>" +
            "</div>"
            if (link.status == 'in progress') {
                document.getElementById(link.id).style.display = 'block';
                document.getElementById(link.id + 'finish').style.display = 'none';
            } else if(link.status == 'waiting') {
                document.getElementById(link.id + 'finish').style.display = 'none';
            } else if (link.status == 'Downloaded'){
                document.getElementById(link.id + 'finish').style.display = 'inline-block';
            }
        });  
    });
}